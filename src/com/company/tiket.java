package com.company;

class Tiket {
    private String nama;
    private String asal;
    private String tujuan;

    // Default constructor
    public Tiket() {
    }

    // Overload constructor
    public void Ticket(String nama) {
        this.nama = nama;
    }

    public void Ticket(String nama, String asal, String tujuan) {
        this.nama = nama;
        this.asal = asal;
        this.tujuan = tujuan;
    }

    // Getter dan Setter
    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public String getAsal() {
        return asal;
    }

    public void setAsal(String asal) {
        this.asal = asal;
    }

    public String getTujuan() {
        return tujuan;
    }

    public void setTujuan(String tujuan) {
        this.tujuan = tujuan;
    }

    // Override method toString
    public String toString() {
        if (asal != null && tujuan != null) {
            return "Nama: " + nama + "Asal: " + asal + "Tujuan: " + tujuan;
        } else {
            return "Nama: " + nama;
        }
    }
}
