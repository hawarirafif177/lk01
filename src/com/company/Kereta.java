package com.company;

import java.util.ArrayList;
import java.util.List;

public class Kereta {
    private String nama;
    private int TiketTersedia;
    private List<Ticket> listTiket = new ArrayList<>();

    public Kereta() {
        this.TiketTersedia = 1000;
    }

    public Kereta(String nama, int TiketTersedia) {
        this.nama = nama;
        this.TiketTersedia = TiketTersedia;
    }

    public void tambahTiket(String nama) {
        if (TiketTersedia > 0) {
            listTiket.add(new Ticket(nama));
            TiketTersedia--;
            System.out.println("==============================");
            System.out.println("Tiket berhasil dipesan");
            if (TiketTersedia < 30) {
                System.out.println("Sisa tiket tersedia: " + TiketTersedia);
            }
        } else {
            System.out.println("==============================");
            System.out.println("Kereta telah habis dipesan, silahkan cari jadwal keberangkatan lainnya");
        }
    }

    public void tambahTiket(String nama, String asal, String tujuan) {
        if (this.nama.equals("Jayabaya")) {
            if (this.TiketTersedia > 0) {
                Ticket tiket = new Ticket(nama, asal, tujuan);
                this.listTiket.add(tiket);
                this.TiketTersedia--;
                System.out.println("Tiket berhasil dipesan. Sisa tiket tersedia: " + this.TiketTersedia);
            } else {
                System.out.println("Kereta telah habis dipesan, silahkan cari jadwal keberangkatan lainnya");
            }
        } else {
            System.out.println("Tipe kereta tidak dikenali");
        }
    }

    public void tampilkanTiket() {
        System.out.println("==============================");
        System.out.println("Daftar penumpang kereta api " + this.nama + " :");
        System.out.println("==============================");
        if (listTiket.isEmpty()) {
            System.out.println("Tidak ada tiket yang dipesan");
        } else {
            for (Ticket tiket : listTiket) {
                System.out.println(tiket.toString());
            }
        }
    }
}
