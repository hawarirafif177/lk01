package com.company;

public class Ticket {
    private String nama;
    private String asal;
    private String tujuan;

    public Ticket() {
    }

    public Ticket(String nama) {
        this.nama = nama;
    }

    public Ticket(String nama, String asal, String tujuan) {
        this.nama = nama;
        this.asal = asal;
        this.tujuan = tujuan;
    }

    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public String getAsal() {
        return asal;
    }

    public void setAsal(String asal) {
        this.asal = asal;
    }

    public String getTujuan() {
        return tujuan;
    }

    public void setTujuan(String tujuan) {
        this.tujuan = tujuan;
    }

    public String toString() {
        if (asal != null && tujuan != null) {
            return "Nama: " + nama + "\nAsal: " + asal + "\nTujuan: " + tujuan;
        } else {
            return "Nama: " + nama;
        }
    }
}
